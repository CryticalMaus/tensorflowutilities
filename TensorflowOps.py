import tensorflow as tf
from tensorflow.python.training import moving_averages


def lrelu(x, leak=0.2, scope: str="lrelu"):
    with tf.name_scope(name=scope, values=[x]):
        f1 = 0.5 * (1 + leak)
        f2 = 0.5 * (1 - leak)
        return f1 * x + f2 * abs(x)


def max_pool(image: tf.Tensor, kernel_size=[2, 2], stride:int =2, padding='VALID', scope=None):
    with tf.name_scope(scope, 'max_pool', [image]):
        return tf.nn.max_pool(image,
                       ksize=[1, kernel_size[0], kernel_size[1], 1],
                       strides=[1, stride, stride, 1],
                       padding=padding)



def conv2d(image: tf.Tensor,
			filter_shape: list,
			out_channel_depth: int,
			stride: int,
			padding='SAME',
			activation=tf.nn.relu,
			stddev: float=0.1,
			bias: float=0.0,
			weight_decay: float=0,
			batch_norm=None,
			trainable=True,
			scope=None,
			reuse=None):
	with tf.variable_scope(scope, 'conv2d', [image], reuse=reuse):
		in_channel_depth = image.get_shape()[-1]
		filter_shape = filter_shape + [in_channel_depth, out_channel_depth]
		l2_regularizer = None
		if weight_decay and weight_decay > 0:
			l2_regularizer = tf.contrib.layers.l2_regularizer(weight_decay)
		initializer = tf.truncated_normal_initializer(stddev=stddev)
		filter = tf.get_variable("filter_weights", filter_shape, tf.float32,
								 initializer, l2_regularizer, trainable)

		conv = tf.nn.conv2d(image, filter, [1, stride, stride, 1],
							padding=padding)

		if batch_norm is None:
			bias_shape = [out_channel_depth, ]
			bias_initializer = tf.constant_initializer(bias)
			biases = tf.get_variable('biases',
										shape=bias_shape,
										initializer=bias_initializer,
										trainable=trainable)
			outputs = tf.nn.bias_add(conv, biases)
		else:
			outputs = batch_norm(conv)
		if activation:
			outputs = activation(outputs)
		return outputs


def inv_conv2d(image: tf.Tensor,
				out_shape: list,
				out_channel_depth: int,
				filter_shape: list,
				stride: int,
				padding='SAME',
				activation=None,
				stddev: float=0.1,
				bias: float=0.0,
				weight_decay: float=0,
				batch_norm=None,
				trainable=True,
				scope=None,
				reuse=None):
	"""

	:param image: [batch, height, width, channels]
	:param out_channel_depth:
	:param filter_shape: [height, width]
	:param out_shape:
	:param stride:
	:param padding:
	:param activation:
	:param stddev:
	:param bias:
	:param weight_decay:
	:param batch_norm:
	:param trainable:
	:param scope:
	:param reuse:
	:return:
	"""
	with tf.variable_scope(scope, 'inv_conv2d', [image], reuse=reuse):
		in_channel_depth = image.get_shape()[-1]
		filter_shape = filter_shape + [out_channel_depth, in_channel_depth]
		l2_regularizer = None
		if weight_decay and weight_decay > 0:
			l2_regularizer = tf.contrib.layers.l2_regularizer(weight_decay)

		initializer = tf.truncated_normal_initializer(stddev=stddev)
		filter = tf.get_variable("filter_weights", filter_shape, tf.float32,
								 initializer, l2_regularizer, trainable)

		batch_size_tensor = tf.shape(image)[0]
		out_shape_tensor = tf.stack([batch_size_tensor, out_shape[0], out_shape[1], out_channel_depth])
		conv = tf.nn.conv2d_transpose(image, filter, out_shape_tensor, [1, stride, stride, 1],
							padding=padding)

		if batch_norm is None:
			bias_shape = [out_channel_depth, ]
			bias_initializer = tf.constant_initializer(bias)
			biases = tf.get_variable('biases',
										shape=bias_shape,
										initializer=bias_initializer,
										trainable=trainable)
			outputs = tf.nn.bias_add(conv, biases)
		else:
			outputs = batch_norm(conv)
		if activation:
			outputs = activation(outputs)
		return outputs


def dense(vector: tf.Tensor,
		  out_channel_depth: int,
		  activation=None,
		  stddev: float=0.01,
		  bias: float=0.0,
		  weight_decay: float=0,
		  batch_norm=None,
		  trainable=True,
		  scope=None,
		  reuse=None):
	with tf.variable_scope(scope, 'dense', [vector], reuse=reuse):
		in_channel_depth = vector.get_shape()[-1]
		filter_shape = [in_channel_depth, out_channel_depth]
		l2_regularizer = None
		if weight_decay and weight_decay > 0:
			l2_regularizer = tf.contrib.layers.l2_regularizer(weight_decay)
		initializer = tf.truncated_normal_initializer(stddev=stddev)
		filter = tf.get_variable("filter_weights", filter_shape, tf.float32,
								 initializer, l2_regularizer, trainable)

		output = tf.matmul(vector, filter)

		if batch_norm is None:
			bias_shape = [out_channel_depth, ]
			bias_initializer = tf.constant_initializer(bias)
			biases = tf.get_variable('biases',
										shape=bias_shape,
										initializer=bias_initializer,
										trainable=trainable)
			output = tf.nn.bias_add(output, biases)
		else:
			output = batch_norm(output)
		if activation:
			output = activation(output)
		return output


class BatchNorm:
	"""
	create object with parameters, call to wrap tensor
	"""
	def __init__(self,
				decay=0.999,
				center=True,
				scale=False,
				epsilon=0.001,
				moving_vars='moving_vars',
				activation=None,
				is_training=True,
				trainable=True,
				scope=None,
				reuse=None):
		self.decay = decay
		self.center = center
		self.scale = scale
		self.epsilon = epsilon
		self.moving_vars = moving_vars
		self.is_training = is_training
		self.activation = activation
		self.trainable = trainable
		self.scope = scope
		self.reuse = reuse

	def __call__(self, image_or_vector: tf.Tensor):
		inputs_shape = image_or_vector.get_shape()
		with tf.variable_scope(self.scope, 'batch_norm', [image_or_vector], reuse=self.reuse):
			axis = list(range(len(inputs_shape) - 1))
			params_shape = inputs_shape[-1:]
			# Allocate parameters for the beta and gamma of the normalization.
			beta, gamma = None, None
			if self.center:
				beta = tf.get_variable('beta',
										  params_shape,
										  initializer=tf.zeros_initializer(),
										  trainable=self.trainable)
			if self.scale:
				gamma = tf.get_variable('gamma',
											params_shape,
											initializer=tf.ones_initializer(),
											trainable=self.trainable)
			# Create moving_mean and moving_variance add them to
			# GraphKeys.MOVING_AVERAGE_VARIABLES collections.
			moving_collections = [self.moving_vars, tf.GraphKeys.MOVING_AVERAGE_VARIABLES]
			moving_mean = tf.get_variable('moving_mean',
											 params_shape,
											 initializer=tf.zeros_initializer(),
											 trainable=False,
											 collections=moving_collections)
			moving_variance = tf.get_variable('moving_variance',
												 params_shape,
												 initializer=tf.ones_initializer(),
												 trainable=False,
												 collections=moving_collections)
			if self.is_training:
				# Calculate the moments based on the individual batch.
				mean, variance = tf.nn.moments(image_or_vector, axis)

				update_moving_mean = moving_averages.assign_moving_average(
					moving_mean, mean, self.decay)
				tf.add_to_collection('_update_ops_', update_moving_mean)
				update_moving_variance = moving_averages.assign_moving_average(
					moving_variance, variance, self.decay)
				tf.add_to_collection('_update_ops_', update_moving_variance)
			else:
				# Just use the moving_mean and moving_variance.
				mean = moving_mean
				variance = moving_variance
			# Normalize the activations.
			outputs = tf.nn.batch_normalization(
				image_or_vector, mean, variance, beta, gamma, self.epsilon)
			outputs.set_shape(image_or_vector.get_shape())
			if self.activation:
				outputs = self.activation(outputs)
			return outputs


def custom_LSTM_cell():
    """
    https://adriancolyer.files.wordpress.com/2017/05/neural-architecture-search-fig-8.jpeg?w=849
    :return:
    """
    raise ValueError("haven't implemented this yet")
    pass