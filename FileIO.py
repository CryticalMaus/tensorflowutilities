import csv


def write_csv_table(filename, table):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        [writer.writerow(r) for r in table]


def read_csv_table(filename, table):
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        table = [[int(e) for e in r] for r in reader]

'''
os.walk
'''
for root, dirs, files in os.walk(apkLocation + apkFolder):
    for name in files:
        if name.endswith(("lib", ".so")):
            os.path.join(root, name)